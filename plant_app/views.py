from django.shortcuts import render
from rest_framework import generics, viewsets, views, status, filters
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from django_filters import rest_framework as dj_filters

from .serializers import *
from .permissions import ProfessorOnly, ReadOnly
from .filter import PlantFilter


class PlantViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows plants to be viewed.
    Safe methods available for everyone.
    Unsafe methods only available for admins and professors.
    """

    queryset = Plant.objects.all()
    serializer_class = PlantSerializer
    # permission_classes = (ProfessorOnly|ReadOnly,)
    permission_classes = (IsAdminUser | ProfessorOnly | ReadOnly,)
    filter_backends = (
        dj_filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    search_fields = ("id", "latin_name", "polish_name", "name_synonyms")
    ordering_fields = (
        "id",
        "latin_name",
        "polish_name",
        "family_latin_name",
        "family_polish_name",
        "name_synonyms",
        "group_raunkiaer",
        "group_geohist",
    )
    # filterset_fields = ("id", "name_latin", "name_polish", "family", "description",)
    filterset_class = PlantFilter
