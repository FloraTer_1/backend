from django.test import TestCase
from django.urls import reverse


class TestURLs(TestCase):
    def test_plant_list(self):
        self.assertEqual(reverse("plants-list"), "/api/plants/")

    def test_plant_detail(self):
        self.assertEqual(reverse("plants-detail", kwargs={"pk": 1}), "/api/plants/1/")

    def test_plant_detail_plus(self):
        self.assertEqual(reverse("plants-detail", kwargs={"pk": 5}), "/api/plants/5/")
