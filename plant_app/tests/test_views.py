from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model

from rest_framework.test import APIClient, APIRequestFactory, force_authenticate
from plant_app.views import PlantViewSet


class TestPlantViewSet(TestCase):
    def setUp(self):
        # create Plant data
        self.latin_name = "Abies alba Mill."
        self.polish_name = "Jodła pospolita"

        self.family_latin_name = "Pinaceae"
        self.family_polish_name = "Sosnowate"

        self.name_synonyms = ""

        self.group_raunkiaer = "M"
        self.group_geohist = "Sn"

        self.data = {
            "latin_name": self.latin_name,
            "polish_name": self.polish_name,
            "family_latin_name": self.family_latin_name,
            "family_polish_name": self.family_polish_name,
            "name_synonyms": self.name_synonyms,
            "group_raunkiaer": self.group_raunkiaer,
            "group_geohist": self.group_geohist,
        }

        # create user data
        self.admin_data = {
            "email": "superuser@florater.one",
            "password": "florater",
            "first_name": "florater",
            "last_name": "admin",
        }
        self.user = get_user_model()
        self.user.objects.create_superuser(**self.admin_data)
        self.owner = self.user.objects.get(email="superuser@florater.one")

        self.client = APIClient()
        self.client.force_authenticate(user=self.owner)
        self.url = reverse("plants-list")

    def test_plant_get(self):
        response = self.client.get(self.url)
        assert response.status_code == 200

    def test_plant_post(self):
        response = self.client.post(self.url, self.data, format="json")
        assert response.status_code == 201

    def test_plant_bad_post(self):
        bad_data = {"group_raunkiaer": "exceeding_maximum_length"}
        response = self.client.post(self.url, bad_data, format="json")
        assert response.status_code == 400

    def test_plant_bad_post_duplicate(self):
        bad_data = {"latin_name": "Abies alba Mill."}
        response = self.client.post(self.url, self.data, format="json")
        response = self.client.post(self.url, bad_data, format="json")
        assert response.status_code == 400

    def test_plant_get_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("plants-detail", kwargs={"pk": 1})
        response = self.client.get(url)
        assert response.status_code == 200

    def test_plant_delete_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("plants-detail", kwargs={"pk": 1})
        response = self.client.delete(url)
        assert response.status_code == 204

    def test_plant_put_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("plants-detail", kwargs={"pk": 1})
        response = self.client.put(url, data=self.data, format="json")
        assert response.status_code == 200

    def test_plant_patch_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("plants-detail", kwargs={"pk": 1})
        new_data = {"name_synonyms": "new_synonym"}
        response = self.client.patch(url, data=new_data, format="json")
        assert response.status_code == 200
