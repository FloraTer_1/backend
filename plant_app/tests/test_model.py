from django.test import TestCase
from plant_app.models import Plant


class TestPlantModel(TestCase):
    def setUp(self):
        self.latin_name = "Abies alba Mill."
        self.polish_name = "Jodła pospolita"

        self.family_latin_name = "Pinaceae"
        self.family_polish_name = "Sosnowate"

        self.name_synonyms = ""

        self.group_raunkiaer = "M"
        self.group_geohist = "Sn"

        self.data = {
            "latin_name": self.latin_name,
            "polish_name": self.polish_name,
            "family_latin_name": self.family_latin_name,
            "family_polish_name": self.family_polish_name,
            "name_synonyms": self.name_synonyms,
            "group_raunkiaer": self.group_raunkiaer,
            "group_geohist": self.group_geohist,
        }
        self.plant = Plant
        self.instance = Plant.objects.create(**self.data)

    def test_plant_instance(self):
        self.assertEqual(self.plant, Plant)

    def test_plant_creation(self):
        self.assertEqual(self.plant.objects.count(), 1)

    def test_plant_creation_multiple(self):
        self.plant.objects.create(latin_name="Acer campestre L.")
        self.assertEqual(self.plant.objects.count(), 2)

    def test_latin_name_exists(self):
        self.assertEqual(self.instance.latin_name, self.data["latin_name"])

    def test_get_name(self):
        self.assertEqual(str(self.instance), self.data["latin_name"])
