from django.apps import apps
from django.test import TestCase
from plant_app.apps import PlantAppConfig


class ConfigTest(TestCase):
    def test_name(self):
        self.assertEqual(PlantAppConfig.name, "plant_app")
        self.assertEqual(apps.get_app_config("plant_app").name, "plant_app")
