from rest_framework import permissions


class ReadOnly(permissions.BasePermission):
    """
    Permission to allow read access for everyone
    """

    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS


class ProfessorOnly(permissions.BasePermission):
    """
    Permission to only professors everything
    """

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return request.user.is_prof()
