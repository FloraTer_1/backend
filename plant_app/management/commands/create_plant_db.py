from django.core.management.base import BaseCommand, CommandError
from plant_app.models import Plant

import csv


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("file_locations", nargs="+")

    def handle(self, *arg, **options):
        for file_location in options["file_locations"]:
            self.stdout.write("Loading file from " + file_location)
            try:
                with open(file_location, encoding="utf-8") as file:
                    self.stdout.write("Adding plants now.")
                    reader = csv.reader(file)
                    for row in reader:
                        _, created = Plant.objects.get_or_create(
                            latin_name=row[0],
                            polish_name=row[1],
                            family_latin_name=row[3],
                            family_polish_name=row[4],
                            name_synonyms=row[2],
                            group_raunkiaer=row[5],
                            group_geohist=row[6],
                        )
                self.stdout.write("Plants have been added successfully!")
            except:
                self.stdout.write("Something unexpected occurred.")
        self.stdout.write("Command finished!")
