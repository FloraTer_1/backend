from rest_framework import serializers
from .models import Plant


class PlantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plant
        fields = (
            "id",
            "latin_name",
            "polish_name",
            "family_latin_name",
            "family_polish_name",
            "name_synonyms",
            "group_raunkiaer",
            "group_geohist",
        )
