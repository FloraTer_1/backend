from django.db import models


class Plant(models.Model):

    latin_name = models.CharField(max_length=100, unique=True, default="test_plant")
    polish_name = models.CharField(max_length=100, default="no_polish_name")

    family_latin_name = models.CharField(max_length=100, default="no_family_latin_name")
    family_polish_name = models.CharField(
        max_length=100, default="no_family_polish_name"
    )

    name_synonyms = models.CharField(max_length=250, blank=True, default="")

    group_raunkiaer = models.CharField(max_length=10, default="unknown")
    group_geohist = models.CharField(max_length=10, default="unknown")

    def __str__(self):
        return self.latin_name
