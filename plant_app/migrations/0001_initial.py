# Generated by Django 2.0.7 on 2019-06-15 20:37

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Plant",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "latin_name",
                    models.CharField(default="test_plant", max_length=100, unique=True),
                ),
                (
                    "polish_name",
                    models.CharField(default="no_polish_name", max_length=100),
                ),
                (
                    "family_latin_name",
                    models.CharField(default="no_family_latin_name", max_length=100),
                ),
                (
                    "family_polish_name",
                    models.CharField(default="no_family_polish_name", max_length=100),
                ),
                (
                    "name_synonyms",
                    models.CharField(blank=True, default="", max_length=250),
                ),
                ("group_raunkiaer", models.CharField(default="unknown", max_length=10)),
                ("group_geohist", models.CharField(default="unknown", max_length=10)),
            ],
        )
    ]
