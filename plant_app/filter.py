from django_filters import rest_framework as filters
from .models import Plant


class PlantFilter(filters.FilterSet):
    class Meta:
        model = Plant
        fields = {
            "id": ["exact"],
            "latin_name": ["icontains"],
            "polish_name": ["icontains"],
            "family_latin_name": ["icontains"],
            "family_polish_name": ["icontains"],
            "name_synonyms": ["icontains"],
            "group_raunkiaer": ["exact"],
            "group_geohist": ["exact"],
        }
