# FloraTer_1 backend

## Requirements
Python 3.6.*  
  
## Installation

#### Create and activate virtual enviroment  

##### Linux

```
python3 -m venv <venv_name>
source <venv_name>/bin/activate
```

##### Windows - Powershell

```
python -m venv <venv_name>
.\<venv_name>\Scripts\Activate.ps1
```  

#### Clone and move into new project  

```
git clone https://gitlab.com/FloraTer_1/backend
cd backend    
```

#### Install required packages

```
pip install -r requirements.txt
```

use ``` pipreqs . ``` to generate new requirements  
  
### Django  
  
#### Create superuser  

```
python manage.py create_su    
```

#### Create plant database from file  

```
python manage.py create_plant_db gatunki.csv     
```

#### Run tests

```
python manage.py test  
```

#### Run server

```
python manage.py runserver  
```

#### Django main site

http://127.0.0.1:8000/

#### Django admin site

http://127.0.0.1:8000/admin/

## Deployment
  
Currently deployed on:  
http://kfsz.pythonanywhere.com/api/users/  
  
Swagger docs available on:  
http://kfsz.pythonanywhere.com/api/swagger/  

ReDoc available on:  
http://kfsz.pythonanywhere.com/api/redoc/
