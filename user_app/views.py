from django.contrib.auth import get_user_model
from django.shortcuts import render
from rest_framework import generics, viewsets, views, status, filters
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from django_filters import rest_framework as dj_filters

from .serializers import *

import uuid


class UserList(generics.ListAPIView):
    """
    API endpoint that allows users to be viewed.
    Returns all users for staff; otherwise only professor users.
    """

    serializer_class = UserSemiSafeSerializer
    filter_backends = (dj_filters.DjangoFilterBackend, filters.SearchFilter)
    search_fields = ("id", "first_name", "last_name", "index")
    filter_fields = ("id", "first_name", "last_name", "index")

    def get_queryset(self):
        queryset = get_user_model().objects.filter()
        if (
            self.request.user.is_staff is False
            and self.request.user.user_type is not "PROF"
        ):
            queryset = queryset.filter(user_type="PROF")
        return queryset


class UserData(generics.ListAPIView):
    """
    API endpoint that allows current user to be viewed.
    """

    serializer_class = UserSemiSafeSerializer

    def list(self, request):
        serializer = UserSemiSafeSerializer(request.user)
        return Response(serializer.data)


class Logout(generics.GenericAPIView):
    """
    API endpoint that allows users to logout.
    Invalidates existing JWT token.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user = request.user
        user.jwt_secret_key = uuid.uuid4()
        user.save()
        return Response(status=status.HTTP_200_OK)


class Register(generics.GenericAPIView):
    """
    API endpoint that allows users to register.
    Requires email, password, first name and last name.
    All fields are required.
    """

    permission_classes = (AllowAny,)
    serializer_class = UserRegistrationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            user = serializer.save()
            if user:
                return Response(status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChangePassword(generics.UpdateAPIView):
    """
    API endpoint that allows users to change their password.
    Requires old password for double authentication.
    """

    permission_classes = (IsAuthenticated,)
    serializer_class = ChangePasswordSerializer

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        user = self.request.user

        if serializer.is_valid():
            # probably not necessary though
            if user.check_password(serializer.data.get("old_password")):
                user.set_password(serializer.data.get("new_password"))
                user.save()
                return Response(status.HTTP_200_OK)

        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
