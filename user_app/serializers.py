from django.contrib.auth import get_user_model
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = (
            "email",
            "password",
            "first_name",
            "last_name",
            "index",
            "user_type",
            "date_joined",
            "is_staff",
            "is_admin",
        )

    # use validated_data for creation
    def create(self, validated_data):
        index = "-"
        if "index" in validated_data:
            index = validated_data["index"]
        user = get_user_model().objects.create_user(
            email=validated_data["email"],
            first_name=validated_data["first_name"],
            last_name=validated_data["last_name"],
            password=validated_data["password"],
            index=index,
        )

        return user


# skip showing password hashes
class UserSemiSafeSerializer(UserSerializer):
    class Meta:
        model = get_user_model()
        fields = (
            "email",
            "first_name",
            "last_name",
            "index",
            "user_type",
            "date_joined",
            "is_staff",
            "is_admin",
        )


# skip showing password hashes and permissions
class UserSafeSerializer(UserSerializer):
    class Meta:
        model = get_user_model()
        fields = (
            "email",
            "first_name",
            "last_name",
            "index",
            "user_type",
            "date_joined",
        )


# skip showing permissions
class UserRegistrationSerializer(UserSerializer):
    class Meta:
        model = get_user_model()
        fields = ("email", "password", "first_name", "last_name", "index")


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
