from django.urls import include, path
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from . import views

urlpatterns = [
    path("", views.UserList.as_view(), name="view_users"),
    path("user-data/", views.UserData.as_view(), name="view_user_data"),
    path("register/", views.Register.as_view(), name="auth_register"),
    path("obtain-token/", obtain_jwt_token, name="auth_token"),
    path("refresh-token/", refresh_jwt_token, name="refresh_auth_token"),
    path("invalidate-token/", views.Logout.as_view(), name="invalidate_auth_token"),
    path(
        "change-password/", views.ChangePassword.as_view(), name="auth_change_password"
    ),
]
