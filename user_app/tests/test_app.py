from django.apps import apps
from django.test import TestCase
from user_app.apps import UserAppConfig


class ConfigTest(TestCase):
    def test_name(self):
        self.assertEqual(UserAppConfig.name, "user_app")
        self.assertEqual(apps.get_app_config("user_app").name, "user_app")
