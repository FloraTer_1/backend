from django.core.management import call_command
from django.test import TestCase
from django.contrib.auth import get_user_model
from io import StringIO


class CommandTest(TestCase):
    def setUp(self):
        self.out = StringIO()

    def test_creation(self):
        call_command("create_su", stdout=self.out)
        self.assertIn("FloraTer_1 superuser created successfully!", self.out.getvalue())

    def test_when_exists(self):
        get_user_model().objects.create_superuser(
            email="superuser@florater.one",
            password="florater",
            first_name="florater",
            last_name="admin",
        )
        call_command("create_su", stdout=self.out)
        self.assertIn("FloraTer_1 superuser already exists!", self.out.getvalue())
