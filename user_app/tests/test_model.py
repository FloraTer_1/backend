from django.test import TestCase
from django.contrib.auth import get_user_model
from user_app.models import User


class TestUserModel(TestCase):
    def setUp(self):
        self.email = "example@example.com"
        self.password = "test"
        self.first_name = "test"
        self.last_name = "test"

        self.data = {
            "email": self.email,
            "password": self.password,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }
        self.user = get_user_model()
        self.instance = get_user_model().objects.create_user(**self.data)

    def test_user_instance(self):
        self.assertEqual(self.user, User)

    def test_user_creation(self):
        self.assertEqual(self.user.objects.count(), 1)

    def test_email_exists(self):
        self.assertEqual(self.instance.email, self.data["email"])

    def test_get_name(self):
        self.assertEqual(str(self.instance), self.data["email"])

    def test_not_enough_fields(self):
        with self.assertRaises(TypeError):
            self.user.objects.create_user(email=self.email, password=self.password)

    def test_superuser_creation(self):
        self.admin_data = {
            "email": "superuser@florater.one",
            "password": "florater",
            "first_name": "florater",
            "last_name": "admin",
        }
        self.user.objects.create_superuser(**self.admin_data)
        number_of_superusers = self.user.objects.filter(is_staff=True).count()
        self.assertEqual(number_of_superusers, 1)
