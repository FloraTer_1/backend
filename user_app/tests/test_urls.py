from django.test import TestCase
from django.urls import reverse


class TestURLs(TestCase):
    def test_list(self):
        self.assertEqual(reverse("view_users"), "/api/users/")

    def test_token(self):
        self.assertEqual(reverse("auth_token"), "/api/users/obtain-token/")

    def test_refresh_token(self):
        self.assertEqual(reverse("refresh_auth_token"), "/api/users/refresh-token/")

    def test_invalidate_token(self):
        self.assertEqual(
            reverse("invalidate_auth_token"), "/api/users/invalidate-token/"
        )

    def test_register(self):
        self.assertEqual(reverse("auth_register"), "/api/users/register/")

    def test_change_password(self):
        self.assertEqual(reverse("auth_change_password"), "/api/users/change-password/")

    def test_user_data(self):
        self.assertEqual(reverse("view_user_data"), "/api/users/user-data/")
