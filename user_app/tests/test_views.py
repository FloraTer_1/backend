from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.conf import settings
from django.core import mail
import re


class TestRegistration(APITestCase):
    def setUp(self):
        self.email = "example@example.com"
        self.password = "test"
        self.first_name = "test"
        self.last_name = "test"

        self.bad_email = "not@email"
        self.bad_password = ""

        self.data = {
            "email": self.email,
            "password": self.password,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }

        self.url = reverse("auth_register")
        self.User = get_user_model()
        self.response = self.client.post(self.url, self.data, format="json")

    def test_register_ok(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_creation(self):
        self.assertEqual(self.User.objects.count(), 1)

    def test_bad_email(self):
        self.data = {
            "email": self.bad_email,
            "password": self.password,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }
        response = self.client.post(self.url, self.data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_bad_password(self):
        self.data = {
            "email": self.email,
            "password": self.bad_password,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }
        response = self.client.post(self.url, self.data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestLogin(APITestCase):
    def setUp(self):
        self.email = "example@example.com"
        self.password = "test"
        self.first_name = "test"
        self.last_name = "test"

        self.bad_email = "not@email"
        self.bad_password = ""

        self.data = {
            "email": self.email,
            "password": self.password,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }

        user = get_user_model().objects.create_user(**self.data)
        user.save()

        self.url = reverse("auth_token")
        self.response = self.client.post(self.url, self.data, format="json")

    def test_response_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_token_return(self):
        self.assertIn("token", self.response.data)

    def test_bad_email(self):
        bad_data = {
            "email": self.bad_email,
            "password": self.password,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }
        response = self.client.post(self.url, bad_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_bad_password(self):
        bad_data = {
            "email": self.email,
            "password": self.bad_password,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }
        response = self.client.post(self.url, bad_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestLogout(APITestCase):
    def setUp(self):
        self.email = "example@example.com"
        self.password = "test"
        self.first_name = "test"
        self.last_name = "test"

        self.data = {
            "email": self.email,
            "password": self.password,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }

        self.user = get_user_model().objects.create_user(**self.data)
        self.client.force_authenticate(user=self.user)

    def test_logout(self):
        prev_jwt_key = self.user.jwt_secret_key
        url = reverse("invalidate_auth_token")
        self.response = self.client.get(url)
        self.user.refresh_from_db()
        new_jwt_key = self.user.jwt_secret_key
        self.assertTrue(prev_jwt_key != new_jwt_key)


class TestUserData(APITestCase):
    def setUp(self):
        self.email = "example@example.com"
        self.password = "test"
        self.first_name = "test"
        self.last_name = "test"

        self.data = {
            "email": self.email,
            "password": self.password,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }

        self.user = get_user_model().objects.create_user(**self.data)
        self.client.force_authenticate(user=self.user)
        url = reverse("view_user_data")
        self.response = self.client.get(url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_response_data(self):
        self.assertEqual(self.response.data["email"], self.user.email)
        self.assertEqual(self.response.data["first_name"], self.user.first_name)
        self.assertEqual(self.response.data["last_name"], self.user.last_name)
