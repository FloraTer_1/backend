from user_app.models import User
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    def handle(self, *arg, **options):
        try:
            User.objects.get(email="superuser@florater.one").save()
            self.stdout.write("FloraTer_1 superuser already exists!")
        except:
            User.objects.create_superuser(
                email="superuser@florater.one",
                password="florater",
                first_name="florater",
                last_name="admin",
            )
        self.stdout.write("FloraTer_1 superuser created successfully!")
