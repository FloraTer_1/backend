from django.urls import include, path
from rest_framework import routers
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r"main", views.ReportViewSet, base_name="reports")
router.register(r"plants", views.ReportedPlantViewSet, base_name="reported_plants")
router.register(r"photos", views.PlantPhotoViewSet, base_name="reported_plant_photos")
router.register(r"groups", views.GroupViewSet, base_name="groups")

urlpatterns = router.urls
urlpatterns += [
    path("upload-photo/", views.PlantPhotoUploadView.as_view(), name="upload-photo")
]
