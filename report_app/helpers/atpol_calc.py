import math

ZERO_E = 19
ZERO_N = 52
ZERO_E_RADIANS = math.radians(ZERO_E)
ZERO_N_RADIANS = math.radians(ZERO_N)
EARTH_RADIUS = 6390


def get_char(val):
    if val < 100:
        return "A"
    elif val < 200:
        return "B"
    elif val < 300:
        return "C"
    elif val < 400:
        return "D"
    elif val < 500:
        return "E"
    elif val < 600:
        return "F"
    elif val < 700:
        return "G"
    else:
        return "X"


def get_10km_int(val):
    return int((val % 100) / 10)


def get_5km_int(val):
    return int((val % 100 % 10) / 5)


def get_1km_int(val):
    return val % 100 % 10


def get_01km_int(val, full_val):
    return int((full_val - val) * 10)


def get_grid(lat, lon):
    # calculate x and y
    lon = lon - ZERO_E
    lat_radians = math.radians(lat)
    lon_radians = math.radians(lon)
    radius = math.cos(ZERO_N_RADIANS) / math.sin(ZERO_N_RADIANS) - math.tan(
        lat_radians - ZERO_N_RADIANS
    )
    x = EARTH_RADIUS * (radius * math.sin(lon_radians * math.sin(ZERO_N_RADIANS))) + 330
    y = (
        EARTH_RADIUS
        * (
            radius * math.cos(lon_radians * math.sin(ZERO_N_RADIANS))
            - math.cos(ZERO_N_RADIANS) / math.sin(ZERO_N_RADIANS)
        )
        + 350
    )
    x_int = int(x)
    y_int = int(y)
    # get grid info
    return [
        str(get_char(x_int))
        + str(get_char(y_int))
        + str(get_10km_int(y_int))
        + str(get_10km_int(x_int))
        + str(get_1km_int(y_int))
        + str(get_1km_int(x_int))
        + str(get_01km_int(y_int, y))
        + str(get_01km_int(x_int, x)),
        str(get_char(x_int))
        + str(get_char(y_int))
        + str(get_10km_int(y_int))
        + str(get_10km_int(x_int))
        + str(get_5km_int(y_int))
        + str(get_5km_int(x_int)),
    ]
