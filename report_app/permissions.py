from rest_framework import permissions
from .models import Report, ReportedPlant


class OptionsOnly(permissions.BasePermission):
    """
    Permission to always allow OPTIONS
    """

    def has_permission(self, request, view):
        return request.method in ["OPTIONS"]

    def has_object_permission(self, request, view, obj):
        return request.method in ["OPTIONS"]


class OwnReportPermission(permissions.BasePermission):
    """
    Permission to only allow adding reports for authenticated users
    """

    def has_permission(self, request, view):
        return request.user.is_authenticated

    """
    Object-level permission to only allow working on user's own reports
    """

    def has_object_permission(self, request, view, obj):
        if obj.owner:
            return obj.owner == request.user
        else:
            return False


class OwnReportedPlantPermission(permissions.BasePermission):
    """
    Permission to only allow adding reported plants for authenticated users
    """

    def has_permission(self, request, view):
        return request.user.is_authenticated

    """
    Object-level permission to only allow working on user's own reported plants
    """

    def has_object_permission(self, request, view, obj):
        if obj.report and obj.report.owner:
            return obj.report.owner == request.user
        else:
            return False


class OwnReportPhotoPermission(permissions.BasePermission):
    """
    Permission to only allow adding photos for for authenticated users
    """

    def has_permission(self, request, view):
        return request.user.is_authenticated

    """
    Object-level permission to only allow working on user's own photos
    """

    def has_object_permission(self, request, view, obj):
        if obj.plant and obj.plant.report and obj.plant.report.owner:
            return obj.plant.report.owner == request.user
        else:
            return False


class OwnGroupPermission(permissions.BasePermission):
    """
    Permission to only allow adding groups for professors
    """

    def has_permission(self, request, view):
        return request.user.is_authenticated and request.user.user_type == "PROF"

    """
    Object-level permission to only allow working on professor's own reports
    """

    def has_object_permission(self, request, view, obj):
        if obj.professor:
            return obj.professor == request.user
        else:
            return False
