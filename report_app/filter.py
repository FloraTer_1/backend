from django_filters import rest_framework as filters
from .models import Report, ReportedPlant


class ReportFilter(filters.FilterSet):
    class Meta:
        model = Report
        fields = {
            "id": ["exact"],
            "group": ["icontains"],
            "place": ["icontains"],
            "habitat": ["icontains"],
            "user_date": ["lt", "gt"],
            "location": ["icontains"],
            "atpol_grid": ["icontains"],
            "atpol_grid_5x5": ["icontains"],
            "comment": ["icontains"],
            "date": ["lt", "gt"],
            "owner__first_name": ["icontains"],
            "owner__last_name": ["icontains"],
            "owner__index": ["exact"],
            "owner__user_type": ["exact"],
        }


class ReportedPlantFilter(filters.FilterSet):
    class Meta:
        model = ReportedPlant
        fields = {
            "id": ["exact"],
            "latin_name": ["icontains"],
            "polish_name": ["icontains"],
            "herbarium": ["exact"],
            "resources": ["lt", "gt", "exact"],
            "extra_info": ["icontains"],
            "report": ["exact"],
        }
