from rest_framework import serializers
from .models import Report, ReportedPlant, PlantPhoto, Group

from plant_app.models import Plant
from plant_app.serializers import PlantSerializer
from user_app.serializers import UserSafeSerializer


class PlantPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlantPhoto
        fields = "__all__"


class PlantPhotoLiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlantPhoto
        fields = ("id", "public_id", "url", "secure_url")


class ReportedPlantSerializer(serializers.ModelSerializer):
    report = serializers.ReadOnlyField(source="report.id")

    details = PlantSerializer(read_only=True)
    photo = PlantPhotoLiteSerializer(many=True, read_only=True)

    class Meta:
        model = ReportedPlant
        fields = (
            "id",
            "latin_name",
            "polish_name",
            "herbarium",
            "resources",
            "photo",
            "extra_info",
            "details",
            "report",
        )


class ReportSerializer(serializers.ModelSerializer):
    owner = UserSafeSerializer(read_only=True)

    plants = ReportedPlantSerializer(many=True, required=False)

    class Meta:
        model = Report
        fields = (
            "id",
            "group",
            "place",
            "habitat",
            "user_date",
            "location",
            "atpol_grid",
            "atpol_grid_5x5",
            "comment",
            "date",
            "owner",
            "plants",
        )

    def create(self, validated_data):
        plants_data = validated_data.pop("plants", None)
        report = Report.objects.create(**validated_data)
        if plants_data:
            for plant_data in plants_data:
                ReportedPlant.objects.create(report=report, **plant_data)
        return report


class GroupSerializer(serializers.ModelSerializer):
    professor = UserSafeSerializer(read_only=True)
    students = UserSafeSerializer(many=True, required=False, read_only=True)

    class Meta:
        model = Group
        fields = ("id", "number", "specialization", "professor", "students")
