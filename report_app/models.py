from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.validators import FileExtensionValidator

from .helpers import atpol_calc

from plant_app.models import Plant

User = get_user_model()


class Report(models.Model):

    group = models.CharField(max_length=30, default="unknown")
    place = models.CharField(max_length=60)
    habitat = models.CharField(max_length=60, default="unknown")
    user_date = models.DateField(blank=True, null=True)
    location = models.CharField(max_length=90, default="gps_not_available")
    atpol_grid = models.CharField(max_length=20, default="atpol_not_available")
    atpol_grid_5x5 = models.CharField(max_length=20, default="atpol_not_available")
    comment = models.TextField(blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(
        User,
        related_name="reporter",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        default=None,
    )

    def save(self, *args, **kwargs):
        try:
            x, y = self.location.split(" ")
            self.atpol_grid, self.atpol_grid_5x5 = atpol_calc.get_grid(
                float(x), float(y)
            )
        except:
            pass
        super(Report, self).save(*args, **kwargs)

    def __str__(self):
        return self.place


class ReportedPlant(models.Model):

    latin_name = models.CharField(max_length=100, default="no_latin_name")
    polish_name = models.CharField(max_length=100, default="no_polish_name")
    herbarium = models.BooleanField(default=False)
    resources = models.IntegerField(
        choices=list(zip(range(1, 4), range(1, 4))), default=1
    )
    extra_info = models.TextField(blank=True, null=True)
    report = models.ForeignKey(
        "Report",
        related_name="plants",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        default=None,
    )
    details = models.ForeignKey(
        "plant_app.Plant",
        related_name="details",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        default=None,
    )

    def __str__(self):
        return self.latin_name


class PlantPhoto(models.Model):

    public_id = models.CharField(max_length=90, default="not_available")
    url = models.CharField(max_length=190, default="not_available")
    secure_url = models.CharField(max_length=190, default="not_available")
    created_at = models.DateTimeField(blank=True, null=True)
    bytes = models.CharField(max_length=60, default="unknown")

    plant = models.ForeignKey(
        "ReportedPlant",
        related_name="photo",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        default=None,
    )

    def __str__(self):
        return self.public_id


class Group(models.Model):

    number = models.CharField(max_length=10)
    specialization = models.CharField(max_length=20)
    professor = models.ForeignKey(
        User, related_name="professor", on_delete=models.CASCADE
    )
    students = models.ManyToManyField(User, related_name="students")

    def __str__(self):
        return self.specialization + " - " + self.number
