from django.contrib.auth import get_user_model
from django.shortcuts import render
from rest_framework import generics, viewsets, views, status, filters
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from django.shortcuts import get_object_or_404
from django_filters import rest_framework as dj_filters

from .serializers import *
from .permissions import (
    OwnReportPermission,
    OwnReportedPlantPermission,
    OwnReportPhotoPermission,
    OwnGroupPermission,
    OptionsOnly,
)
from .filter import ReportFilter, ReportedPlantFilter
from .utils import cloudinary_upload

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from .custom_auth import JSONWebTokenAuthenticationQS

from collections import Counter

from plant_app.models import Plant

import datetime
import django_excel as excel


class ReportViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows reports to be viewed.
    Permissions - adding for everyone, 
                  viewable are only user's own.
    Object permissions - only for user's own reports
    """

    serializer_class = ReportSerializer
    permission_classes = (OwnReportPermission | OptionsOnly,)
    authentication_classes = (
        JSONWebTokenAuthenticationQS,
        SessionAuthentication,
        BasicAuthentication,
    )
    filter_backends = (
        dj_filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    search_fields = (
        "id",
        "group",
        "place",
        "habitat",
        "user_date",
        "location",
        "atpol_grid",
        "atpol_grid_5x5",
        "comment",
        "date",
    )
    ordering_fields = (
        "id",
        "group",
        "place",
        "habitat",
        "user_date",
        "location",
        "atpol_grid",
        "atpol_grid_5x5",
        "comment",
        "date",
    )
    filterset_class = ReportFilter

    def get_queryset(self):
        """
        Only returns reports linked to user's account
            or reports linked to selected group for professors
        """
        if self.request.user.is_authenticated and self.request.user.user_type == "PROF":
            all = self.request.query_params.get("all")
            if all:
                return Report.objects.filter()
            group_id = self.request.query_params.get("group_id")
            if group_id:
                try:
                    group = Group.objects.get(id=group_id, professor=self.request.user)
                    return Report.objects.filter(owner__in=group.students.all())
                except:
                    pass
            return Report.objects.filter(owner=self.request.user)
        elif self.request.user.is_authenticated:
            return Report.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        if self.request.user.is_authenticated:
            serializer.save(owner=self.request.user)

    @action(methods=["get"], detail=False)
    def analysis(self, request):
        queryset = self.get_queryset()
        analysis = {}
        analysis["reports_count"] = queryset.count()

        groups = []
        places = []
        habitats = []
        locations = []

        atpol = []
        atpol_5x5 = []

        plants_count = 0
        plants_names_polish = []
        plants_names_latin = []
        plants_family_names_polish = []
        plants_family_names_latin = []

        plants_unconfirmed_names_polish = []
        plants_unconfirmed_names_latin = []

        contributors = []

        for cur_report in queryset:
            groups.append(cur_report.group)
            places.append(cur_report.place)
            habitats.append(cur_report.habitat)
            locations.append(cur_report.location)
            atpol.append(cur_report.atpol_grid)
            atpol_5x5.append(cur_report.atpol_grid_5x5)
            contributors.append(cur_report.owner.get_name())
            for plant in ReportedPlant.objects.filter(report=cur_report):
                plants_count += 1
                if plant.details:
                    plants_names_polish.append(plant.details.polish_name)
                    plants_names_latin.append(plant.details.latin_name)
                    plants_family_names_polish.append(plant.details.family_polish_name)
                    plants_family_names_latin.append(plant.details.family_latin_name)
                else:
                    plants_unconfirmed_names_polish.append(plant.polish_name)
                    plants_unconfirmed_names_latin.append(plant.latin_name)

        analysis["groups"] = Counter(groups)
        analysis["places"] = Counter(places)
        analysis["habitats"] = Counter(habitats)
        analysis["locations"] = Counter(locations)
        analysis["atpol"] = Counter(atpol)
        analysis["atpol_5x5"] = Counter(atpol_5x5)
        analysis["plants_count"] = plants_count
        analysis["plants_names_polish"] = Counter(plants_names_polish)
        analysis["plants_names_latin"] = Counter(plants_names_latin)
        analysis["plants_family_names_polish"] = Counter(plants_family_names_polish)
        analysis["plants_family_names_latin"] = Counter(plants_family_names_latin)
        analysis["plants_unconfirmed_names_polish"] = Counter(
            plants_unconfirmed_names_polish
        )
        analysis["plants_unconfirmed_names_latin"] = Counter(
            plants_unconfirmed_names_latin
        )
        analysis["contributors"] = Counter(contributors)

        return Response(analysis)

    @action(methods=["get"], detail=False)
    def excel(self, request):
        queryset = ReportedPlant.objects.filter(report__in=self.get_queryset())
        column_names = [
            "latin_name",
            "polish_name",
            "herbarium",
            "resources",
            "extra_info",
            "details__latin_name",
            "details__polish_name",
            "details__family_latin_name",
            "details__family_polish_name",
            "details__name_synonyms",
            "details__group_raunkiaer",
            "details__group_geohist",
            "report__group",
            "report__place",
            "report__habitat",
            "report__user_date",
            "report__location",
            "report__atpol_grid",
            "report__atpol_grid_5x5",
            "report__comment",
            "report__date",
            "report__owner__email",
            "report__owner__first_name",
            "report__owner__last_name",
            "report__owner__index",
            "report__owner__user_type",
        ]
        return excel.make_response_from_query_sets(
            queryset,
            column_names,
            "xlsx",
            file_name="plant_data-" + str(datetime.datetime.now()),
        )


class ReportedPlantViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows reported plants to be viewed.
    Permissions - only allow adding reported plants to user's own reports, 
                  viewable are only user's own.
    Object permissions - only for user's own reports.
    """

    serializer_class = ReportedPlantSerializer
    permission_classes = (OwnReportedPlantPermission | OptionsOnly,)
    authentication_classes = (
        JSONWebTokenAuthenticationQS,
        SessionAuthentication,
        BasicAuthentication,
    )
    filter_backends = (
        dj_filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    search_fields = (
        "id",
        "latin_name",
        "polish_name",
        "herbarium",
        "resources",
        "extra_info",
    )
    ordering_fields = (
        "id",
        "latin_name",
        "polish_name",
        "herbarium",
        "resources",
        "extra_info",
        "report",
    )
    filterset_class = ReportedPlantFilter

    def get_queryset(self):
        """
        Only returns reports linked to user's account
            or reported plants linked to selected group for professors
        """
        if self.request.user.is_authenticated and self.request.user.user_type == "PROF":
            all = self.request.query_params.get("all")
            if all:
                return ReportedPlant.objects.filter()
            group_id = self.request.query_params.get("group_id")
            if group_id:
                try:
                    group = Group.objects.get(id=group_id, professor=self.request.user)
                    return ReportedPlant.objects.filter(
                        report__owner__in=group.students.all()
                    )
                except:
                    pass
            return ReportedPlant.objects.filter(report__owner=self.request.user)
        elif self.request.user.is_authenticated:
            return ReportedPlant.objects.filter(report__owner=self.request.user)

    def perform_create(self, serializer):
        report = self.request.data.get("report", None)
        photos = self.request.data.get("photos", None)

        latin_name = self.request.data.get("latin_name", None)
        polish_name = self.request.data.get("polish_name", None)

        # try to link with plant model
        # waiting for python 3.8
        plant = Plant.objects.filter(latin_name=latin_name).first()
        if plant is not None:
            serializer.save(details=plant)
        else:
            plant = Plant.objects.filter(polish_name=polish_name).first()
            if plant is not None:
                serializer.save(details=plant)

        # try to link with report
        if report and self.request.user.is_authenticated:
            report = get_object_or_404(Report, id=report, owner=self.request.user)
            serializer.save(report=report)

        reported_plant = serializer.save()

        # try to save photos
        if photos:
            for photo in photos:
                photo = get_object_or_404(PlantPhoto, id=photo)
                photo.plant = reported_plant
                photo.save()

    @action(methods=["get"], detail=False)
    def analysis(self, request):
        reports_ids = self.get_queryset().values_list("report_id", flat=True)
        queryset = Report.objects.filter(id__in=reports_ids)

        analysis = {}
        analysis["reports_count"] = queryset.count()

        groups = []
        places = []
        habitats = []
        locations = []

        atpol = []
        atpol_5x5 = []

        plants_count = 0
        plants_names_polish = []
        plants_names_latin = []
        plants_family_names_polish = []
        plants_family_names_latin = []

        plants_unconfirmed_names_polish = []
        plants_unconfirmed_names_latin = []

        contributors = []

        for cur_report in queryset:
            groups.append(cur_report.group)
            places.append(cur_report.place)
            habitats.append(cur_report.habitat)
            locations.append(cur_report.location)
            atpol.append(cur_report.atpol_grid)
            atpol_5x5.append(cur_report.atpol_grid_5x5)
            contributors.append(cur_report.owner.get_name())
            for plant in ReportedPlant.objects.filter(report=cur_report):
                plants_count += 1
                if plant.details:
                    plants_names_polish.append(plant.details.polish_name)
                    plants_names_latin.append(plant.details.latin_name)
                    plants_family_names_polish.append(plant.details.family_polish_name)
                    plants_family_names_latin.append(plant.details.family_latin_name)
                else:
                    plants_unconfirmed_names_polish.append(plant.polish_name)
                    plants_unconfirmed_names_latin.append(plant.latin_name)

        analysis["groups"] = Counter(groups)
        analysis["places"] = Counter(places)
        analysis["habitats"] = Counter(habitats)
        analysis["locations"] = Counter(locations)
        analysis["atpol"] = Counter(atpol)
        analysis["atpol_5x5"] = Counter(atpol_5x5)
        analysis["plants_count"] = plants_count
        analysis["plants_names_polish"] = Counter(plants_names_polish)
        analysis["plants_names_latin"] = Counter(plants_names_latin)
        analysis["plants_family_names_polish"] = Counter(plants_family_names_polish)
        analysis["plants_family_names_latin"] = Counter(plants_family_names_latin)
        analysis["plants_unconfirmed_names_polish"] = Counter(
            plants_unconfirmed_names_polish
        )
        analysis["plants_unconfirmed_names_latin"] = Counter(
            plants_unconfirmed_names_latin
        )
        analysis["contributors"] = Counter(contributors)

        return Response(analysis)

    @action(methods=["get"], detail=False)
    def excel(self, request):
        queryset = self.get_queryset()
        column_names = [
            "latin_name",
            "polish_name",
            "herbarium",
            "resources",
            "extra_info",
            "details__latin_name",
            "details__polish_name",
            "details__family_latin_name",
            "details__family_polish_name",
            "details__name_synonyms",
            "details__group_raunkiaer",
            "details__group_geohist",
            "report__group",
            "report__place",
            "report__habitat",
            "report__user_date",
            "report__location",
            "report__atpol_grid",
            "report__atpol_grid_5x5",
            "report__comment",
            "report__date",
            "report__owner__email",
            "report__owner__first_name",
            "report__owner__last_name",
            "report__owner__index",
            "report__owner__user_type",
        ]
        return excel.make_response_from_query_sets(
            queryset,
            column_names,
            "xlsx",
            file_name="plant_data-" + str(datetime.datetime.now()),
        )


class PlantPhotoUploadView(views.APIView):
    """
    API endpoint for uploading plant photos.
    Uploads photos to imagekit.io and saves urls.
    Available for everyone.
    """

    parser_class = (FileUploadParser,)
    permission_classes = (OwnReportPhotoPermission | OptionsOnly,)

    def put(self, request, *args, **kwargs):

        # second term - also accept already upload files by url
        photo = request.data["photo"]
        reported_plant = request.data.get("reported_plant", None)

        result = cloudinary_upload(photo)
        plant_photo_serializer = PlantPhotoSerializer(data=result)

        if plant_photo_serializer.is_valid():
            plant_photo_serializer.save()
            # try to link with reported plant
            if reported_plant and request.user.is_authenticated:
                reported_plant = get_object_or_404(
                    ReportedPlant, id=reported_plant, report__owner=self.request.user
                )
                plant_photo_serializer.save(plant=reported_plant)
            return Response(plant_photo_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                plant_photo_serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )


class PlantPhotoViewSet(viewsets.ModelViewSet):
    """
    API endpoint for viewing and deleting plant photos.
    Available for everyone ATM.
    """

    queryset = PlantPhoto.objects.all()
    permission_classes = (OwnReportPhotoPermission | OptionsOnly,)
    serializer_class = PlantPhotoSerializer
    http_method_names = ["get", "head", "options", "delete"]

    def get_queryset(self):
        """
        Only returns photos linked to user's account
        """
        if self.request.user.is_authenticated:
            return PlantPhoto.objects.filter(plant__report__owner=self.request.user)


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed, created or updated.
    Permissions - only allow adding groups for professors, 
                  all are viewable, but only for professors.
    Object permissions - only for user's own reports.
    """

    serializer_class = GroupSerializer
    permission_classes = (OwnGroupPermission | OptionsOnly,)
    filter_backends = (filters.SearchFilter,)
    search_fields = ("number", "specialization")

    def get_queryset(self):
        """
        Only returns groups linked to user's account
        """
        if self.request.user.is_authenticated:
            return Group.objects.filter(professor=self.request.user)

    def perform_create(self, serializer):
        serializer.save(professor=self.request.user)

    @action(methods=["post"], detail=True)
    def add_student(self, request, pk=None):
        group = self.get_object()
        student_index = request.data["index"]
        student = get_user_model().objects.filter(index=student_index)
        if student:
            group.students.add(*student)
            group.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_404_NOT_FOUND)

    @action(methods=["delete"], detail=True)
    def remove_student(self, request, pk=None):
        group = self.get_object()
        student_index = request.data["index"]
        student = get_user_model().objects.filter(index=student_index)
        if student:
            group.students.remove(*student)
            group.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_404_NOT_FOUND)
