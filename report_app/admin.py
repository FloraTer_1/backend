from django.contrib import admin
from .models import Report, ReportedPlant, PlantPhoto, Group

# Register your models here.

admin.site.register(Report)
admin.site.register(ReportedPlant)
admin.site.register(PlantPhoto)
admin.site.register(Group)
