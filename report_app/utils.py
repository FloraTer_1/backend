from django.conf import settings

import json
import requests


def cloudinary_upload(photo):
    files = {"file": photo}
    data = {"upload_preset": settings.CLOUDINARY_UPLOAD_PRESET}
    response = requests.post(settings.CLOUDINARY_URL, files=files, data=data).json()
    return response
