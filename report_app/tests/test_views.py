from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model

from rest_framework.test import APIClient, APIRequestFactory, force_authenticate
from report_app.views import ReportViewSet, ReportedPlantViewSet


class TestReportViewSet(TestCase):
    def setUp(self):
        # create Report data
        self.group = "group"
        self.place = "place"
        self.habitat = "habitat"
        self.user_date = "2019-06-08"
        self.location = "not_available"

        self.data = {
            "group": self.group,
            "place": self.place,
            "habitat": self.habitat,
            "user_date": self.user_date,
            "location": self.location,
        }

        # create user data
        self.admin_data = {
            "email": "superuser@florater.one",
            "password": "florater",
            "first_name": "florater",
            "last_name": "admin",
        }
        self.user = get_user_model()
        self.user.objects.create_superuser(**self.admin_data)
        self.owner = self.user.objects.get(email="superuser@florater.one")

        self.client = APIClient()
        self.client.force_authenticate(user=self.owner)
        self.url = reverse("reports-list")

    def test_report_get(self):
        response = self.client.get(self.url)
        assert response.status_code == 200

    def test_report_post(self):
        response = self.client.post(self.url, self.data, format="json")
        assert response.status_code == 201

    def test_report_bad_post(self):
        bad_data = {"bad_data": "test"}
        response = self.client.post(self.url, bad_data, format="json")
        assert response.status_code == 400

    def test_report_get_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("reports-detail", kwargs={"pk": 1})
        response = self.client.get(url)
        assert response.status_code == 200

    def test_report_delete_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("reports-detail", kwargs={"pk": 1})
        response = self.client.delete(url)
        assert response.status_code == 204

    def test_report_put_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("reports-detail", kwargs={"pk": 1})
        response = self.client.put(url, data=self.data, format="json")
        assert response.status_code == 200

    def test_report_patch_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("reports-detail", kwargs={"pk": 1})
        new_data = {"location": "new_location_found"}
        response = self.client.patch(url, data=new_data, format="json")
        assert response.status_code == 200


class TestReportedPlantViewSet(TestCase):
    def setUp(self):

        # create ReportedPlant data
        self.latin_name = "latin_name"
        self.polish_name = "polish_name"
        self.herbarium = False
        self.resources = 2

        self.data = {
            "latin_name": self.latin_name,
            "polish_name": self.polish_name,
            "herbarium": self.herbarium,
            "resources": self.resources,
            "report": 1,
        }

        # create Report data
        self.group = "group"
        self.place = "place"
        self.habitat = "habitat"
        self.user_date = "2019-06-08"
        self.location = "not_available"

        self.report_data = {
            "group": self.group,
            "place": self.place,
            "habitat": self.habitat,
            "user_date": self.user_date,
            "location": self.location,
        }

        # create user data
        self.admin_data = {
            "email": "superuser@florater.one",
            "password": "florater",
            "first_name": "florater",
            "last_name": "admin",
        }
        self.user = get_user_model()
        self.user.objects.create_superuser(**self.admin_data)
        self.owner = self.user.objects.get(email="superuser@florater.one")

        self.url = reverse("reported_plants-list")
        self.report_url = reverse("reports-list")

        self.client = APIClient()
        self.client.force_authenticate(user=self.owner)
        self.client.post(self.report_url, self.report_data, format="json")

    def test_report_get(self):
        response = self.client.get(self.url)
        assert response.status_code == 200

    def test_report_post(self):
        response = self.client.post(self.url, self.data, format="json")
        assert response.status_code == 201

    def test_report_bad_post(self):
        bad_data = {"herbarium": "string"}
        response = self.client.post(self.url, bad_data, format="json")
        assert response.status_code == 400

    def test_report_get_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("reported_plants-detail", kwargs={"pk": 1})
        response = self.client.get(url)
        assert response.status_code == 200

    def test_report_delete_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("reported_plants-detail", kwargs={"pk": 1})
        response = self.client.delete(url)
        assert response.status_code == 204

    def test_report_put_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("reported_plants-detail", kwargs={"pk": 1})
        response = self.client.put(url, data=self.data, format="json")
        assert response.status_code == 200

    def test_report_patch_detail(self):
        self.client.post(self.url, self.data, format="json")
        url = reverse("reported_plants-detail", kwargs={"pk": 1})
        new_data = {"herbarium": True}
        response = self.client.patch(url, data=new_data, format="json")
        assert response.status_code == 200
