from django.test import TestCase
from django.urls import reverse


class TestURLs(TestCase):
    def test_report_list(self):
        self.assertEqual(reverse("reports-list"), "/api/reports/main/")

    def test_report_detail(self):
        self.assertEqual(
            reverse("reports-detail", kwargs={"pk": 1}), "/api/reports/main/1/"
        )

    def test_reported_plant_list(self):
        self.assertEqual(reverse("reported_plants-list"), "/api/reports/plants/")

    def test_reported_plant_detail(self):
        self.assertEqual(
            reverse("reported_plants-detail", kwargs={"pk": 1}),
            "/api/reports/plants/1/",
        )

    def test_reported_plant_detail_plus(self):
        self.assertEqual(
            reverse("reported_plants-detail", kwargs={"pk": 5}),
            "/api/reports/plants/5/",
        )

    def test_upload(self):
        self.assertEqual(reverse("upload-photo"), "/api/reports/upload-photo/")
