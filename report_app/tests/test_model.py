from django.test import TestCase
from report_app.models import Report, ReportedPlant


class TestReportModel(TestCase):
    def setUp(self):
        self.group = "group"
        self.place = "place"
        self.habitat = "habitat"
        self.user_date = "2019-06-08"
        self.location = "not_available"

        self.data = {
            "group": self.group,
            "place": self.place,
            "habitat": self.habitat,
            "user_date": self.user_date,
            "location": self.location,
        }
        self.report = Report
        self.instance = Report.objects.create(**self.data)

    def test_report_instance(self):
        self.assertEqual(self.report, Report)

    def test_report_creation(self):
        self.assertEqual(self.report.objects.count(), 1)

    def test_report_creation_multiple(self):
        self.report.objects.create(group=self.group)
        self.assertEqual(self.report.objects.count(), 2)

    def test_place_exists(self):
        self.assertEqual(self.instance.place, self.data["place"])

    def test_get_name(self):
        self.assertEqual(str(self.instance), self.data["place"])


class TestReportedPlantModel(TestCase):
    def setUp(self):
        # create Report
        self.group = "group"
        self.place = "place"
        self.habitat = "habitat"
        self.user_date = "2019-06-08"
        self.location = "not_available"

        self.data_report = {
            "group": self.group,
            "place": self.place,
            "habitat": self.habitat,
            "user_date": self.user_date,
            "location": self.location,
        }
        self.report = Report
        self.instance_report = Report.objects.create(**self.data_report)

        # create ReportedPlant
        self.latin_name = "latin_name"
        self.polish_name = "polish_name"
        self.herbarium = True
        self.resources = 2

        self.data = {
            "latin_name": self.latin_name,
            "polish_name": self.polish_name,
            "herbarium": self.herbarium,
            "resources": self.resources,
            "report": self.instance_report,
        }
        self.reported_plant = ReportedPlant
        self.instance = ReportedPlant.objects.create(**self.data)

    def test_reported_plant_instance(self):
        self.assertEqual(self.reported_plant, ReportedPlant)

    def test_reported_plant_creation(self):
        self.assertEqual(self.report.objects.count(), 1)

    def test_reported_plant_creation_multiple(self):
        self.reported_plant.objects.create(
            latin_name=self.latin_name, polish_name=self.polish_name
        )
        self.assertEqual(self.reported_plant.objects.count(), 2)

    def test_latin_name_exists(self):
        self.assertEqual(self.instance.latin_name, self.data["latin_name"])

    def test_get_name(self):
        self.assertEqual(str(self.instance), self.data["latin_name"])

    def test_report_link(self):
        self.assertEqual(self.instance_report, self.instance.report)
