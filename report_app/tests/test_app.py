from django.apps import apps
from django.test import TestCase
from report_app.apps import ReportAppConfig


class ConfigTest(TestCase):
    def test_name(self):
        self.assertEqual(ReportAppConfig.name, "report_app")
        self.assertEqual(apps.get_app_config("report_app").name, "report_app")
